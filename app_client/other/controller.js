(function () {     // Building controllers for api http requests 

  // asssign controllers to myApp module 
  angular.module('myApp').controller('TestController', TestController);
  
  angular.module('myApp').controller('FacController', FacController);
  angular.module('myApp').controller('StuController', StuController);
  
  angular.module('myApp').controller('GetOneFacController', GetOneFacController);
  angular.module('myApp').controller('GetOneStuController', GetOneStuController);
  
  
  function TestController() {
    var myModel = this;
    myModel.name = 'Test';
  
  }
  
  function FacController($http) {
    
    // for testing 
    var myModel = this;
    myModel.name = 'Faculty';
    
    $http.get('/api/faculty').then(function(response) {   // use .then instead of .success
        myModel.response = response;
       
      });
       
  }
 
  // Notice input parameter changes from $http to StudentFactory
  function StuController(StudentFactory) {
      
      // instead of running this code, 
      // would need StuController($http)   
      // var myModel = this;
      // myModel.name = 'Students:';
      // $http.get('/api/students').then(function(response) {   
      //     myModel.response = response;  
      // });
      
      // we will use StudentFactory
      var vm = this; // controller  calls the service  
      StudentFactory.getAllStudents().then(function(response) { 
        vm.students = response; 
        
        });
   }
 
   function GetOneStuController($routeParams, StudentFactory) {
      
      var vm = this;
      var id = $routeParams.id; 
      StudentFactory.getOneStudent(id).then(function(response) { 
        vm.student = response; 
        
        });
   }
 
   function GetOneFacController($http, $routeParams) {
    var vm = this;   // vm ViewModel variable 
    
    //console.log($routeParams);
    
    var id =  $routeParams.id; 
    
    $http.get('/api/faculty/' + id)
      .then(function(response) {  
        vm.faculty = response.data; 
       
      });
       
  }
 
})();